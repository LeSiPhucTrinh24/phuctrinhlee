package com.peter.dp;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SinhVienReader {
	public static List<SinhVien> readFromFile(String fileName) throws IOException{
		List<SinhVien> lst = new ArrayList<>();
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		while(true) {
			String line = br.readLine();
			if(line == null)
					break;
			//tách chuỗi
			String[] info = line.split(",");
			if(info.length < 3)
				continue;
			String MaSinhVien = info[0];
			String HoTen = info[1];
			String stDiemTrungBinh = info[2];
			double DiemTrungBinh = Double.parseDouble(stDiemTrungBinh);
			
			SinhVien x = new SinhVien(MaSinhVien, HoTen, DiemTrungBinh);
			lst.add(x);
		}
		br.close();
		return lst;
	}
}
